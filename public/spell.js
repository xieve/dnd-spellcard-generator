import { type } from "./util.js";
export { Spell as default };

export class Spell {
  name;
  level;
  school;
  casting_time;
  range;
  _components;
  duration;
  description;
  _classes;
  _src = new Set();

  set components(components) {
    // Add missing spaces in components ("V,S,M" -> "V, S, M")
    this._components = components.replaceAll(/,(?=\S)/g, ", ");
  }

  get components() {
    return this._components;
  }

  set classes(classes) {
    // Convert classes array to Set so all elements are unique
    this._classes = new Set(classes);
  }

  get classes() {
    return this._classes;
  }

  set src(src) {
    this._src = new Set(src);
  }

  get src() {
    return this._src;
  }

  constructor(spell_data) {
    Object.assign(this, spell_data);

    // Trim every value that is a string
    Object.entries(this).map(([key, value]) => {
      if (type(value, "String")) this[key] = value.trim();
    });
  }

  /**
   * Translate and trim class names. Currently only supports German and English.
   * @param {string|Set|Array} classes
   * @returns {string|Set}
   */
  static translate_classes(classes) {
    const translate_str = (class_) => {
      [
        // TODO: i18n
        ["magier", "wizard"],
        ["kleriker", "cleric"],
        ["hexenmeister", "warlock"],
        ["druide", "druid"],
        ["zauberer", "sorcerer"],
        ["waldl\u00e4ufer", "ranger"],
        ["waldläufer", "ranger"],
        ["barde", "bard"],
        [/ .*/, ""], // Removes subclasses for icon compatibility
      ].forEach(
        ([substr, replace]) =>
          (class_ = class_.toLowerCase().replace(substr, replace).trim())
      );
      return class_;
    };
    if (type(classes, "Set")) {
      // idk
      return new Set([...classes].map(translate_str));
    } else if (type(classes, "String")) {
      return translate_str(classes);
    }
  }

  static translate_school(school) {
    [
      ["bannzauber", "abjuration"],
      ["beschwörung", "conjuration"],
      ["erkenntniszauber", "divination"],
      ["verzauberung", "enchantment"],
      ["hervorrufung", "evocation"],
      ["nekromantie", "necromancy"],
      ["verwandlung", "transmutation"],
    ].forEach(
      ([substr, replace]) =>
        (school = school.toLowerCase().replace(substr, replace))
    );
    return school;
  }

  toJSON() {
    const serializable = Object.assign({}, this);
    Object.keys(serializable).forEach((k) => {
      if (k.startsWith("_")) delete serializable[k];
    });
    if (this.classes) serializable.classes = [...this.classes];
    if (this.src) serializable.src = [...this.src];
    serializable.components = this.components;
    return serializable;
  }
}
