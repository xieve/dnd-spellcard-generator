/**
 * Check if `value` is of type `type_str`
 * @param value
 * @param {string} type_str
 * @returns {boolean}
 */
export function type(value, type_str) {
  return Object.prototype.toString.call(value) === `[object ${type_str}]`;
}

/**
 * Returns input string with first letter capitalized
 * @param {string} str
 * @returns {string}
 */
export function capitalize_first(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * If `condition` does not apply, throw an error with `message`
 * @param {boolean} condition
 * @param {string} message
 */
export function assert(condition, message) {
  if (!condition) {
    message = message || "Assertion failed";
    if (typeof Error !== "undefined") {
      throw new Error(message);
    }
    throw message; // Fallback
  }
}

/**
 * @param {number} i
 * @returns {string} i with st, nd, rd or th appended as appropriate
 */
export function append_ordinal_suffix(i) {
  var j = i % 10,
    k = i % 100;
  if (j === 1 && k !== 11) {
    return i + "st";
  }
  if (j === 2 && k !== 12) {
    return i + "nd";
  }
  if (j === 3 && k !== 13) {
    return i + "rd";
  }
  return i + "th";
}

/**
 * Simple CSV parser
 * @param {string} csv_str      String delimited CSV string
 * @param {string} delimiter    Delimiter override
 * @attribution: http://www.greywyvern.com/?post=258
 */
export function parse_csv(csv_str, delimiter = ";") {
  // http://stackoverflow.com/questions/1155678/javascript-string-newline-character
  const universalNewline = /\r\n|\r|\n/g;
  const csv_lines = csv_str.split(universalNewline);
  const csv_arr = [];
  csv_lines.forEach((line_str) => {
    let line_arr, x, tl;
    for (
      line_arr = line_str.split(delimiter), x = line_arr.length - 1;
      x >= 0;
      x--
    ) {
      if (
        line_arr[x].replace(/"\s+$/, '"').charAt(line_arr[x].length - 1) === '"'
      ) {
        if (
          (tl = line_arr[x].replace(/^\s+"/, '"')).length > 1 &&
          tl.charAt(0) === '"'
        ) {
          line_arr[x] = line_arr[x]
            .replace(/^\s*"|"\s*$/g, "")
            .replace(/""/g, '"');
        } else if (x) {
          line_arr.splice(
            x - 1,
            2,
            [line_arr[x - 1], line_arr[x]].join(delimiter)
          );
        } else line_arr = line_arr.shift().split(delimiter).concat(line_arr);
      } else line_arr[x].replace(/""/g, '"');
    }
    // Discard empty lines
    if (line_arr.length > 0 && line_str !== "") {
      csv_arr.push(line_arr);
    }
  });
  return csv_arr;
}
