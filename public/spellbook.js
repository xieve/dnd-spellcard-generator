import Spell from "./spell.js";
import { capitalize_first, append_ordinal_suffix } from "./util.js";

export { Spellbook as default };

export class Spellbook {
  spells = [];
  strings;
  #spells_by_name = {};

  constructor(spell_data = {}) {
    if (spell_data.spells !== undefined) this.append(...spell_data.spells);
    this.strings = spell_data.strings;
    // console.log(this);
  }

  #update() {
    this.spells = Object.values(this.#spells_by_name).sort((a, b) =>
      a.name.localeCompare(b.name)
    );
  }

  /**
   * Append every `Spell` in `spells` that doesn't exist yet
   * @param {Spell} spells
   */
  append(...spells) {
    spells.forEach((spell) => {
      spell.classes = new Set(spell.classes);

      // Collate spells by name
      if (this.#spells_by_name[spell.name.toLowerCase()]) {
        spell.classes.forEach((class_) =>
          this.#spells_by_name[spell.name.toLowerCase()].classes.add(class_)
        );
      } else {
        this.#spells_by_name[spell.name.toLowerCase()] = new Spell(spell);
      }
    });
    this.#update();
  }

  /**
   * Append cards from our JSON format
   * @param {string} json_str
   */
  append_from_json(json_str) {
    const parsed_json = JSON.parse(json_str);
    if (!this.strings && parsed_json.strings)
      this.strings = parsed_json.strings;
    if (parsed_json.spells)
      this.append(...parsed_json.spells.map((spell) => new Spell(spell)));
  }

  /**
   * Create spellbook from our JSON format
   * @param {string} json_str
   * @returns {Spellbook}
   */
  static from_json(json_str) {
    const spellbook = new Spellbook();
    spellbook.append_from_json(json_str);
    return spellbook;
  }

  /**
   * Append cards from hardcodex.ru CSV
   * (currently only supports German and English)
   * @param {string[][]} csv_arr
   */
  append_from_parsed_csv(csv_arr) {
    csv_arr.forEach((csv_line_arr) => {
      // Deconstruct CSV lines
      // Trim every string before assigning
      let [
        level,
        name,
        school,
        casting_time,
        range,
        components,
        duration,
        description,
        class_,
      ] = csv_line_arr.map((str) => str.trim());
      class_ = Spell.translate_classes(class_);

      // Duration and concentration
      let concentration = false;
      const concentration_match = duration.match(
        /(.*)[ck]on[cz]entration,? ?/i
      );
      if (concentration_match) {
        concentration = true;
        // Remove "Concentration" from duration string
        [, duration] = concentration_match;
      }
      // Capitalize first letter
      duration = capitalize_first(duration);

      // Description and "At Higher Levels"
      let description_at_higher_levels;
      const description_match = description.match(
        /^(.*?) ?(?:<br ?\/?>)? ?(?:<b>)?(?:Auf höheren Graden|At Higher Levels)\.? ?(?:<\/b>)?:? ?(.*?)$/i
      );
      if (description_match) {
        [, description, description_at_higher_levels] = description_match;
      }
      description = description.replace(/^\((.*?)\) ?/, (match, material) => {
        if (material) {
          return `<b>Material</b>: ${material}<br>`;
        }
      });
      // Add new <p> nodes instead of <br>
      description = description
        .split(/<br ?\/?>/)
        .map((substr) => `<p>${substr.trim()}</p>`)
        .join("");

      const spell = {
        name,
        level: parseInt(level),
        school,
        casting_time,
        range,
        components,
        duration,
        description,
        classes: [class_],
      };
      if (description_match)
        spell.description_at_higher_levels = description_at_higher_levels;
      if (concentration) spell.concentration = concentration;

      this.append(new Spell(spell));
    });
  }

  /**
   * Create spellbook from hardcodex.ru CSV
   * (currently only supports German and English)
   * @param {string[][]} csv_arr
   * @returns {Spellbook}
   */
  static from_parsed_csv(csv_arr) {
    const spellbook = new Spellbook();
    spellbook.append_from_parsed_csv(csv_arr);
    return spellbook;
  }

  /**
   * Currently only supports German and English
   * JSON from https://www.dnddeutsch.de/tools/j_spells.php?op=json
   */
  dnddeutsch_json_parser(json_obj) {
    json_obj.data.forEach((spell) => {
      const classes = new Set(spell.classfilter.value.split(","));
      classes.delete("");
      // Remove any html tags (should be all links) from the name
      const name_en = spell.name_en.value.replaceAll(/<.*?>/g, "");
      let new_spell;

      if (this.strings.lang === "de-DE") {
        new_spell = {
          name: spell.name_de.value,
          name_en,
          level: spell.level.value,
          school: `${spell.level.value === "0" ? "Zaubertrick der " : ""}${
            spell.school.value
          }${
            spell.level.value !== "0" ? ` des ${spell.level.value}. Grades` : ""
          }${spell.ritual.class === "yes" ? " (Ritual)" : ""}`,
          components: spell.componentsfilter.value.replace(/,,.*$/, ""),
          // This is fully intentional and correct, the data doesn't indicate duration but casting
          // time despite the name
          casting_time: spell.duration.value,
          concentration: spell.concentration.class === "yes",
          classes: Spell.translate_classes(classes),
          page_de: spell.page_de.value,
          page_en: spell.page_en.value,
          src: new Set(
            spell.src.value.split(",").map((src) => src.trim().toLowerCase())
          ),
        };
      } else {
        new_spell = {
          name: name_en,
          level: spell.level.value,
          school: capitalize_first(
            `${
              spell.level.value !== "0"
                ? `${append_ordinal_suffix(spell.level.value)} level `
                : ""
            }${Spell.translate_school(spell.school.value)}${
              spell.level.value === "0" ? " cantrip" : ""
            }${spell.ritual.class === "yes" ? " (ritual)" : ""}`
          ),
          concentration: spell.concentration.class === "yes",
          classes: Spell.translate_classes(classes),
          page_en: spell.page_en.value,
          src: new Set(
            spell.src.value.split(",").map((src) => src.trim().toLowerCase())
          ),
        };
      }

      if (this.#spells_by_name[new_spell.name.toLowerCase()]) {
        Object.assign(
          this.#spells_by_name[new_spell.name.toLowerCase()],
          new_spell
        );
      } else if (new_spell.name) {
        // Cards without a german name (untranslated) are not added
        this.#spells_by_name[new_spell.name.toLowerCase()] = new Spell(
          new_spell
        );
      }
    });
    this.#update();
  }
}
