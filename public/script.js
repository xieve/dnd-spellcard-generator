import Spellbook from "./spellbook.js";
import { type, parse_csv } from "./util.js";

/**
 * Download a json file containing `data`
 * @param {string} filename
 * @param {Object} data
 */
function create_json_download(filename, data) {
  const element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:application/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(data, null, 2))
  );
  element.setAttribute("download", filename);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();

  document.body.removeChild(element);
}

/**
 * Generate a svg symbol with `selector` and the correct viewport width
 * @param {string} selector Selector for the <use> tag
 * @returns {string} HTML string containing the svg symbol or empty string if
 *                   selector doesn't apply
 */
function generate_svg_symbol_icon(selector) {
  let viewBox;
  try {
    viewBox = document.querySelector(selector).viewBox.baseVal;
  } catch (e) {
    // console.log(e);
    return "";
  }
  return `
    <svg
      xmlns="http://www.w3.org/1999/xhtml"
      class="symbol"
      viewBox="${viewBox.x} ${viewBox.y} ${viewBox.width + 1} ${
    viewBox.height
  }">
      <use href="${selector}"></use>
    </svg>`;
}

/**
 * Reads uploaded file as text
 * @param {File} file
 * @returns {Promise}
 */
function read_file_as_text(file) {
  const reader = new FileReader();

  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort();
      reject(reader.error);
    };

    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsText(file);
  });
}

/**
 * Apply strings defined by `i18n_strings` to `template_card` element
 * @param {DocumentFragment} template_card
 * @param {Object} i18n_strings
 * @param {string} i18n_strings.lang
 * @param {string} i18n_strings.casting_time
 * @param {string} i18n_strings.range
 * @param {string} i18n_strings.components
 * @param {string} i18n_strings.duration
 * @param {string} i18n_strings.at_higher_levels
 * @param {string} i18n_strings.concentration_letter
 */
function apply_lang_to_template(template_card, i18n_strings) {
  [
    [".card-label-casting-time", i18n_strings.casting_time.toUpperCase()],
    [".card-label-range", i18n_strings.range.toUpperCase()],
    [".card-label-components", i18n_strings.components.toUpperCase()],
    [".card-label-duration", i18n_strings.duration.toUpperCase()],
    [".card-label-at-higher-levels", i18n_strings.at_higher_levels],
    [
      ".card-concentration-letter",
      i18n_strings.concentration_letter.toUpperCase(),
    ],
  ].forEach(([selector, new_text]) => {
    template_card.querySelector(selector).textContent = new_text;
  });
  template_card.firstElementChild.setAttribute("lang", i18n_strings.lang);
}

function warn(str) {
  const warnings_box = document.querySelector("#warnings");
  warnings_box.textContent += `${str}\n`;
}

/**
 * @param {DocumentFragment} template_card
 * @param {Spellbook} spellbook
 */
export function create_cards(template_card, spellbook) {
  const cards_div = document.querySelector("#cards-div");
  const warnings_box = document.querySelector("#warnings");

  apply_lang_to_template(template_card, spellbook.strings);

  // Clear old cards
  cards_div.textContent = "";
  // Clear old warnings
  warnings_box.textContent = "";

  spellbook.spells.forEach((spell) => {
    const card_instance = template_card.cloneNode(true);

    let class_symbols_str = "";
    if (type(spell.classes, "Set")) {
      [...spell.classes]
        .sort((a, b) => a.localeCompare(b))
        .forEach((dnd_class) => {
          class_symbols_str += generate_svg_symbol_icon(`#symbol-${dnd_class}`);
        });
    }

    //card_instance.querySelector(".card-description")
    [
      [".card-spell-name", spell.name.toUpperCase()],
      [".card-school-level-text", spell.school],
      [".card-value-casting-time", spell.casting_time],
      [".card-value-range", spell.range],
      [".card-value-components", spell.components],
      [".card-value-duration", spell.duration],
      [
        // Use div.card-description if there's a description for higher levels,
        // div.card-longer-description otherwise
        spell.description_at_higher_levels
          ? "div.card-description"
          : "div.card-longer-description",
        spell.description,
      ],
      [
        "div.card-description-at-higher-levels",
        spell.description_at_higher_levels,
      ],
      [".card-bottom-left-symbols", class_symbols_str],
    ].forEach(([selector, new_text]) => {
      if (!new_text && selector !== "div.card-description-at-higher-levels")
        warn(
          `No text for "${selector}": ${spell.name} (${[...spell.src].join(
            ", "
          )}, page ${spell.page_de || spell.page_en})`
        );
      card_instance.querySelector(selector).innerHTML = (new_text || "").trim();
    });
    if (spell.concentration) {
      card_instance
        .querySelector(".card-concentration")
        .classList.remove("invisible");
    }
    if (!spell.description_at_higher_levels) {
      card_instance
        .querySelector(".card-section-at-higher-levels")
        .classList.add("invisible");
    }

    const svg_node = card_instance.firstElementChild;
    // Append created card to div
    cards_div.append(card_instance);

    // Whenever the document changes, we're checking whether it's our card. If it is, we can
    // compute the actual bounding box.
    const observer = new MutationObserver(function () {
      if (cards_div.contains(svg_node)) {
        console.log("asdasfsf");
        // Fits title to available space, scaling down the font if necessary
        const max_width = 636.58; // Width of the Description boxes
        const spell_name_node = svg_node.querySelector(".card-spell-name");
        const bb = spell_name_node.getBBox();
        const scale = max_width / bb.width;
        if (scale < 1) {
          const original_font_size = parseFloat(
            window
              .getComputedStyle(spell_name_node)
              .getPropertyValue("font-size")
              .replace("px", "")
          );

          spell_name_node.setAttribute(
            "style",
            `font-size: ${scale * original_font_size}px`
          );
        }

        // Detect overfull text divs
        if (
          svg_node.querySelector(".card-description > div").scrollHeight >
          svg_node.querySelector(".card-description > div").clientHeight
        ) {
          warn(`Overfull text box: ${spell.name}`);
        }

        observer.disconnect();
      }
    });

    observer.observe(document, {
      attributes: false,
      childList: true,
      characterData: false,
      subtree: true,
    });
  });
}

/**
 * Retrieve JSON at `url` via fetch() and render cards
 * @param {DocumentFragment} template_card
 * @param {string} url
 * @returns {Promise} for the resulting spellbook object
 */
export function load_spellbook_json(template_card, url) {
  return new Promise((resolve) => {
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .catch((ex) => {
        document.querySelector(
          "#cards-div"
        ).innerHTML = `<p>Oh no! The provided JSON seems to be invalid. Details:</p><code>${ex}</code>`;
        throw ex;
      })
      .then((spellbook_data) => {
        const spellbook = new Spellbook(spellbook_data);
        resolve(spellbook);
        // create_cards(template_card, spellbook);
      });
  });
}

/**
 * Create new cards with all filters in `filter_functions` applied
 * @param {DocumentFragment} template_card
 * @param {Spellbook} original_spellbook
 * @param {Object} filter_functions
 * @returns {Spellbook} filtered Spellbook
 */
function update_cards_filtered(
  template_card,
  original_spellbook,
  filter_functions
) {
  const filtered_spellbook = {};
  Object.assign(filtered_spellbook, original_spellbook);
  Object.values(filter_functions).forEach(
    (fun) => (filtered_spellbook.spells = filtered_spellbook.spells.filter(fun))
  );
  create_cards(template_card, filtered_spellbook);
  return new Spellbook(filtered_spellbook);
}

export async function index_on_dom_content_loaded() {
  // Test to see if the browser supports the HTML template element by checking
  // for the presence of the template element's content attribute.
  if ("content" in document.createElement("template")) {
    const template_card = document.getElementById("template-card").content;
    const lang_select = document.querySelector("#lang-select");
    let original_spellbook = new Spellbook();
    let visible_spellbook = original_spellbook;
    const spellbook_filters = {};

    function update_cards() {
      const loading_anim = document.getElementById("loading-anim-div");
      // Show loading animation
      loading_anim.style.display = "";
      visible_spellbook = update_cards_filtered(
        template_card,
        original_spellbook,
        spellbook_filters
      );
      loading_anim.style.display = "none";
    }

    lang_select.addEventListener("change", async (event) => {
      original_spellbook = await load_spellbook_json(
        template_card,
        event.target.value
      );
      update_cards();
    });

    document
      .querySelector("#upload")
      .addEventListener("change", async (event) => {
        // console.log(event.target.files);
        original_spellbook = new Spellbook();
        // Iterate over the files and read them
        for (const file of event.target.files) {
          const file_content = await read_file_as_text(file);
          if (file.type === "application/json") {
            original_spellbook.append_from_json(file_content);
          } else if (
            file.type === "application/vnd.ms-excel" ||
            file.type === "text/csv"
          ) {
            original_spellbook.append_from_parsed_csv(parse_csv(file_content));
          }
        }
        create_cards(template_card, original_spellbook);
        update_cards();
      });

    document.querySelector("#export").addEventListener("click", () => {
      // This could theoretically lead to a race condition if the user would
      // press the button before all files have loaded
      create_json_download(
        `spellcards_export_${new Date().toISOString()}.json`,
        original_spellbook
      );
    });

    /**
     * Filter spells by whether they have a description
     * @param {HTMLInputElement} checkbox
     */
    function hide_empty(checkbox) {
      if (checkbox.checked) {
        spellbook_filters.no_desc = (spell) => spell.description !== undefined;
      } else {
        delete spellbook_filters.no_desc;
      }
    }

    const hide_empty_chkbox = document.querySelector("#hide-no-description");
    hide_empty_chkbox.addEventListener("change", (event) => {
      hide_empty(event.target);
      update_cards();
    });

    /**
     * Filter spells by level
     * @param {HTMLSelectElement} low lower limit
     * @param {HTMLSelectElement} high upper limit
     */
    function filter_level(low, high) {
      spellbook_filters.level = (spell) =>
        spell.level >= low.value && spell.level <= high.value;
    }

    const level_filter_high = document.querySelector("#level-filter-high");
    const level_filter_low = document.querySelector("#level-filter-low");
    [level_filter_high, level_filter_low].forEach((filter) => {
      filter.addEventListener("change", () => {
        filter_level(level_filter_low, level_filter_high);
        update_cards();
      });
    });

    document.querySelector("#dnddeutsch").addEventListener("click", () => {
      fetch("spells_meta_de_en.json")
        .then((response) => response.json())
        .then((json_obj) => {
          original_spellbook.dnddeutsch_json_parser(json_obj);
          create_cards(template_card, original_spellbook);
          update_cards();
        });
    });

    original_spellbook = await load_spellbook_json(
      template_card,
      lang_select.value
    );
    hide_empty(hide_empty_chkbox);
    filter_level(level_filter_low, level_filter_high);

    const filter_sets = {};

    /**
     * Filter spells by an attribute that is a set (e.g. classes and sources)
     * @param {HTMLDivElement} checkboxes_div
     * @param {HTMLInputElement} checkbox
     * @param {string} attr
     */
    function filter_by_set(checkboxes_div, checkbox, attr) {
      if (filter_sets[attr] === undefined) filter_sets[attr] = new Set();
      if (checkbox !== null) {
        if (checkbox.checked) {
          spellbook_filters[attr] = (spell) => {
            if (!spell[attr])
              warn(`Missing/empty attribute "${attr}": ${spell.name}`);
            return [...filter_sets[attr]].reduce(
              (prev, cur) => prev || spell[attr].has(cur),
              false
            );
          };
          filter_sets[attr].add(checkbox.id);
        } else {
          filter_sets[attr].delete(checkbox.id);
        }
      }
      const dropdown_summary =
        checkboxes_div.parentNode.querySelector("summary");
      const label = dropdown_summary.innerText.replace(
        / \(([0-9]+|All)\)$/,
        ""
      );
      if (filter_sets[attr].size > 0) {
        // Display number of filter items in pseudo-drop-down (details summary)
        dropdown_summary.innerText = `${label} (${
          filter_sets[attr].size >= checkboxes_div.children.length - 1
            ? "All"
            : filter_sets[attr].size
        })`;
      } else {
        dropdown_summary.innerText = label;
        delete spellbook_filters[attr];
      }
    }

    function handle_checkbox_dropdown(attr) {
      const checkboxes_div = document.querySelector(`#${attr}-filter`);
      ["#check-all-", "#uncheck-all-"].forEach((selector_part) => {
        let update_checkbox_and_filter;
        if (selector_part === "#check-all-") {
          update_checkbox_and_filter = (checkbox) => {
            checkbox.checked = true;
            filter_by_set(checkboxes_div, checkbox, attr);
          };
        } else {
          update_checkbox_and_filter = (checkbox) => {
            checkbox.checked = false;
            delete filter_sets[attr];
            filter_by_set(checkboxes_div, null, attr);
          };
        }

        document
          .querySelector(`${selector_part}${attr}`)
          .addEventListener("click", () => {
            [...checkboxes_div.children].forEach((div) => {
              const checkbox = div.querySelector("input");
              if (!checkbox) return;
              update_checkbox_and_filter(checkbox);
            });
            update_cards();
          });
      });

      [...checkboxes_div.children].forEach((div) => {
        const checkbox = div.querySelector("input");
        if (!checkbox || checkbox.id === `all-${attr}`) return;
        checkbox.addEventListener("change", (event) => {
          filter_by_set(checkboxes_div, event.target, attr);
          update_cards();
        });
        filter_by_set(checkboxes_div, checkbox, attr);
      });
    }

    handle_checkbox_dropdown("classes");
    handle_checkbox_dropdown("src");

    update_cards();

    // This is only relevant if the font files aren't cached, we need them for calculating the bbox
    // of the card titles. The callback fixes the titles clipping because they're not properly
    // resized when you first load the page.
    window.addEventListener("load", () => {
      update_cards();
    });
  } else {
    document.body.textContent =
      "Sorry, but your browser is not supported. Please update or switch to the latest Firefox release.";
  }
}
